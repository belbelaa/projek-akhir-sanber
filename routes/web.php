<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// use Illuminate\Routing\Route;

// Login dan Register
Route::get('/register', 'AuthController@getRegister')->name('register');
Route::get('/login', 'AuthController@getLogin')->name('login');
Route::post('/register', 'AuthController@postRegister');
Route::post('/login', 'AuthController@postLogin');
Route::get('/logout', 'AuthController@logout')->name('logout');

Route::get('/', 'AboutController@home')->name('home');
Route::get('/about', 'AboutController@about')->name('about');

// Edit User
Route::get('/user/{user_id}/edit', 'UserController@edit');
Route::put('/user/{user_id}', 'UserController@update');

// restaurant Route dengan query
Route::get('/restaurant/tambahRestaurant', 'PostController@create');
Route::post('/restaurant', 'PostController@store');
Route::get('/restaurant', 'PostController@index');
Route::get('/restaurant/search', 'PostController@search')->name('restaurant.search');
Route::get('/restaurant/{restaurantID}', 'PostController@show');
Route::get('/restaurant/{restaurantID}/edit', 'PostController@edit');
Route::put('/restaurant/{restaurantID}', 'PostController@update');
Route::delete('/restaurant/{restaurantID}', 'PostController@destroy')->name('restaurant.destroy');

// Review
Route::get('/review/{restaurantID}', 'ReviewController@show');
Route::post('/review/{restaurantID}', 'ReviewController@store');
Route::get('/review/{restaurantID}/show', 'ReviewController@showreview');
