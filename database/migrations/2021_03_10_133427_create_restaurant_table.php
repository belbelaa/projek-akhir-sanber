<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRestaurantTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('restaurant', function (Blueprint $table) {
            $table->bigIncrements('restaurantID');
            $table->string('nama_restaurant', 255);
            $table->string('alamat', 255);
            $table->string('kota', 255);
            $table->text('deskripsi', 255);
            $table->string('jam_buka');
            $table->string('fasilitas', 255);
            $table->string('telepon', 255);
            $table->string('image', 255);
            $table->unsignedBigInteger('users_id')->nullable($value = true);
            $table->foreign('users_id')->references('id')->on('users');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('restaurant');
    }
}
