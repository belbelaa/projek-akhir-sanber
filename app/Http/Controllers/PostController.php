<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\PostRestaurant;
use App\Review;

class PostController extends Controller
{

    public function __construct() {
        $this->middleware('auth')->except(['index', 'search']);
    }

    public function create() {
        return view('restaurant.tambahRestaurant');
    }

    public function store(Request $request){
        $PostRestaurant = new PostRestaurant;
        
        $PostRestaurant->nama_restaurant = $request["nama"];
        $PostRestaurant->alamat          = $request["alamat"];
        $PostRestaurant->kota            = $request["kota"];
        $PostRestaurant->deskripsi       = $request["deskripsi"];
        $PostRestaurant->jam_buka        = $request["jambuka"];
        $PostRestaurant->fasilitas       = $request["fasilitas"];
        $PostRestaurant->telepon         = $request["telepon"];
        

        if($request->hasfile('image')) {
            $file = $request->file('image');
            $extension = $file->getClientOriginalExtension();
            $filename = time() . '.' . $extension;
            $file->move('uploads/restaurant/', $filename);
            $PostRestaurant->image = $filename;
        } else {
            return $request;
            $PostRestaurant->image = '';
        }

        $PostRestaurant->save(); 
        return redirect('/restaurant')->with('success', 'Restaurant Berhasil Di Tambahkan');
    }

    public function index(){
        $restaurant = DB::table('restaurant')->get();
        return view('restaurant.index', compact('restaurant'));
    }

    public function search(Request $request){
        $keyword = $request->keyword;

        $restaurant = DB::table('restaurant')
        ->where('nama_restaurant','like',"%". $keyword ."%")
        ->orWhere('alamat','like',"%". $keyword ."%")
        ->orWhere('kota','like',"%". $keyword ."%")
        ->get();
        return view('restaurant.index', compact('restaurant'));
    }

    public function show($restaurantID){
        $restoran = PostRestaurant::find($restaurantID);
        $restoran = DB::table('restaurant')->where('restaurantID', $restaurantID)->first();
        $review = DB::table('review')
        ->where('restaurant_restaurantID', $restaurantID)
        ->leftJoin('users', 'review.users_id', '=', 'users.id')
        ->select('review.reviewID', 'review.users_id', 'review.restaurant_restaurantID', 'review.komentar', 'review.created_at', 'users.name')
        ->get();
        return view('restaurant.detailRestaurant', compact('restoran','review'));
    }

    public function edit($restaurantID) {
        $restoran = PostRestaurant::find($restaurantID);
        return view('restaurant.editRestaurant', compact('restoran'));
    }

    public function update($restaurantID, Request $request) {
        $update = PostRestaurant::where('restaurantID', $restaurantID)->update([
            'nama_restaurant'   => $request["nama"],
            'alamat'            => $request["alamat"],
            'kota'              => $request["kota"],
            'deskripsi'         => $request["deskripsi"],
            'jam_buka'          => $request["jambuka"],
            'fasilitas'         => $request["fasilitas"],
            'telepon'           => $request["telepon"]
        ]);
        return redirect('/restaurant')->with('success', 'Data Berhasil Di Ubah'); 
    }

    public function destroy($restaurantID) {
        PostRestaurant::destroy($restaurantID);
        return redirect('/restaurant')->with('success', 'Restaurant Berhasil Di Hapus');
    }

}
