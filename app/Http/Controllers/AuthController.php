<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;

class AuthController extends Controller
{

    public function __construct() {
        $this->middleware('guest')->except('logout');
    }

    public function getLogin() {
        return view('user.login');
    }

    public function postLogin(Request $request) {
        if (!auth()->attempt(['email' => $request->email, 'password' => $request->password])) {
            return redirect() -> back();
        }
        return redirect()->route('home');
    }
    
    public function getRegister() {
        return view('user.register');
    }

    public function postRegister(Request $request) {
        $this->validate($request, [
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|confirmed'
        ]);

        User::create([
            'name' => $request->name,
            'email' => $request->email,
            'password' => bcrypt($request->password)
        ]);

        return redirect()->route('login')->with('success', 'Congratulation! Your account has been created. Please Login');
    }

    public function logout() {
        auth()->logout();
        return redirect()->route('home');
    }
}
