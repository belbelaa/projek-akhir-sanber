<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class UserController extends Controller
{

    public function __construct() {
        $this->middleware('auth');
    }

    public function edit($id){
        $users = DB::table('users')->where('id', $id)->first();
        return view('user.edit', compact('users'));
    }

    public function update($id, Request $request){
        $request->validate([
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255',
            'password' => 'required|string|confirmed'
        ]);
        $query = DB::table('users')
        ->where('id', $id)
        ->update([
            'name' => $request->name,
            'email' => $request->email,
            'password' => bcrypt($request->password)
        ]);
        return redirect()->route('login')->with('success', 'Congratulation! Your account has been edited. Please Login');
    }

}
