<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Review;
use App\PostRestaurant;
use DB;
use Auth;

class ReviewController extends Controller
{
    public function __construct() {
        $this->middleware('auth')->only(['store', 'show']);
    }

    public function show($restaurantID){
        $restoran = PostRestaurant::find($restaurantID);
        return view('review.create', compact('restoran'));
    }

    public function showreview($restaurant_restaurantID){
        $review = Review::find($restaurant_restaurantID);
    }

    public function store($restaurantID, Request $request){
        $restoran = DB::table('restaurant')->where('restaurantID', $restaurantID)->first();
        $post = new Review;
        $post->restaurant_restaurantID = $restoran->restaurantID;
        $post->komentar = $request->komentar;
        $post->users_id = Auth::id();
        $post->save();
        return redirect('/restaurant')->with('success','review berhasil ditambahkan');
    }
}