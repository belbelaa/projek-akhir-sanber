<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Review extends Model
{
    protected $table = "review";
    protected $primaryKey = 'reviewID';

    protected $fillable = ["restaurant_restaurantID", "users_id", "komentar"];

}
