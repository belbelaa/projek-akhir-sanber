<!DOCTYPE html>
<html lang="en">

<head>
    <!-- <title>@yield('judul')</title> -->
    <title>Zotomat</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link href="https://fonts.googleapis.com/css?family=Nanum+Gothic:400,700,800" rel="stylesheet">
    <link rel="stylesheet" href="../../assets/fonts/icomoon/style.css">
    <link rel="stylesheet" href="../../assets/css/bootstrap.min.css">
    <link rel="stylesheet" href="../../assets/css/magnific-popup.css">
    <link rel="stylesheet" href="../../assets/css/jquery-ui.css">
    <link rel="stylesheet" href="../../assets/css/owl.carousel.min.css">
    <link rel="stylesheet" href="../../assets/css/owl.theme.default.min.css">
    <link rel="stylesheet" href="../../assets/css/bootstrap-datepicker.css">
    <link rel="stylesheet" href="../../assets/fonts/flaticon/font/flaticon.css">
    <link rel="stylesheet" href="../../assets/css/aos.css">
    <link rel="stylesheet" href="../../assets/css/rangeslider.css">
    <link rel="stylesheet" href="../../assets/css/style.css">
    <script src="http://demo.itsolutionstuff.com/plugin/jquery.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.css" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.js"></script>
        <meta name="csrf-token" content="{{ csrf_token() }}">
</head>
<body>
    <div class="site-wrap">
        <header class="site-navbar container py-0 bg-white" role="banner">
            <div class="row align-items-center">
                <div class="col-6 col-xl-2">
                    <h1 class="mb-0 site-logo"><a href="/" class="text-black mb-0">Zoto<span class="text-warning">mat</span> </a></h1>
                </div>
                <div class="col-12 col-md-10 d-none d-xl-block">
                    <nav class="site-navigation position-relative text-right" role="navigation">
                        <ul class="site-menu js-clone-nav mr-auto d-none d-lg-block">
                            <li><a href="/restaurant">Restaurant</a></li>
                            <li class="text-warning">|</li>
                            <li><a href="/about">About</a></li>
                            <li class="text-warning">|</li>
                            @if (auth()->user() != null)
                            @if (auth()->user()->name == "admin")
                            <li><a href="/verifikasiRestaurant">Verifikasi Restaurant</a></li>
                            <li class="text-warning">|</li>
                            @endif
                            <li class="has-children">
                                <a href="#"> {{auth()->user()->name}}
                                </a>
                                <ul class="dropdown">
                                    <li><a href="/user/{{auth()->user()->id}}/edit">Edit Profile</a></li>
                                    <li><a href="{{ route('logout') }}">Log Out</a></li>
                                </ul>
                            </li>
                            @else
                            <li><a href="{{ route('login') }}">Login</a></li>
                            <li class="text-warning">|</li>
                            <li><a href="{{ route('register') }}">Register</a></li>
                            <li class="text-warning">|</li>
                            @endif
                        </ul>
                    </nav>
                </div>
            </div>
        </header>