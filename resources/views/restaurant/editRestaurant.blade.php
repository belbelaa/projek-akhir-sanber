@include('templates.header')

<div class="site-blocks-cover inner-page-cover overlay" style="background-image:url('../../assets/images/register.jpg');" data-aos="fade" data-stellar-background-ratio="0.5">
    <div class="container">
        <div class="row align-items-center justify-content-center text-center">
            <div class="col-md-10" data-aos="fade-up" data-aos-delay="400">
                <div class="row justify-content-center mt-5">
                    <div class="col-md-8 text-center">
                        <h1>Edit Restaurant</h1>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<form role="form" action="/restaurant/{{$restoran->restaurantID}}" method="POST">
    @csrf
    @method('PUT')
    <div class="site-section bg-light">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-md-7 mb-5" data-aos="fade">
                    <div class="row form-group">
                        <div class="col-md-12">
                            <label for="nama" class="text-black">Nama</label>
                            <input type="text" id="nama" name="nama" value="{{ old('nama', $restoran->nama_restaurant) }}" class="form-control" placeholder="Nama Restoran" required>
                            {{-- @error('nama_restaurant')
                                <div class="alert alert-danger">{{ $message }}</div>
                            @enderror --}}
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col-md-12">
                            <label for="alamat" class="text-black">Alamat</label>
                            <input type="text" id="alamat" name="alamat" value="{{ old('alamat', $restoran->alamat) }}" class="form-control" placeholder="Alamat Restaurant" >
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col-md-12">
                            <label for="kota" class="text-black">Kota</label>
                            <input type="text" id="kota" name="kota" value="{{ old('kota', $restoran->kota) }}" class="form-control" placeholder="Kota Restaurant" >
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col-md-12">
                            <label for="jambuka" class="text-black">Jam Buka</label>
                            <input type="text" id="jambuka" name="jambuka" value="{{ old('jambuka', $restoran->jam_buka) }}" class="form-control" placeholder="Jam Buka Restaurant" >
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col-md-12">
                            <label for="fasilitas" class="text-black">Fasilitas</label>
                            <input type="text" id="fasilitas" name="fasilitas" value="{{ old('fasilitas', $restoran->fasilitas) }}" class="form-control" placeholder="Fasilitas Restaurant" >
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col-md-12">
                            <label for="telepon" class="text-black">Nomor Telepon</label>
                            <input type="text" id="telepon" name="telepon" value="{{ old('telepon', $restoran->telepon) }}" class="form-control" placeholder="Nomor Telepon Restaurant" >
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col-md-12">
                            <label for="deskripsi" class="text-black">Deskripsi</label>
                            <input type="text" id="deskripsi" name="deskripsi" value="{{ old('deskripsi', $restoran->deskripsi) }}" class="form-control" placeholder="Deskripsi Restaurant" >
                            {{-- <textarea type="text" id="deskripsi" name="deskripsi" value="{{ old('deskripsi', $restoran->deskripsi) }}" class="form-control" placeholder="Deskripsi Restaurant" >
                            </textarea> --}}
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col-md-12">
                            <input type="submit" value="Edit" name="edit" class="btn btn-success py-2 px-4 text-white">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</form>

@include('templates.footer')
