@include('templates.header')

<div class="site-blocks-cover inner-page-cover overlay" style="background-image: url('../../uploads/restaurant/{{$restoran->image}}');" data-aos="fade" data-stellar-background-ratio="0.5">
    <div class="container">
        <div class="row align-items-center justify-content-center text-center">
            <div class="col-md-10" data-aos="fade-up" data-aos-delay="400">
                <div class="row justify-content-center mt-5">
                    <div class="col-md-8 text-center">
                        <h1>{{ $restoran->nama_restaurant }}</h1>
                        <p class="mb-0">Jalan {{ $restoran->alamat }}</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="site-section">
    <div class="container">
        <div class="row">
            <div class="col-lg-11">
                <h2 class="font-weight-light text-warning">Description</h2> <br><br>
                <p align="justify">{{ $restoran->deskripsi }}</p>
                <p align="justify"> <b> Open at : </b> {{ $restoran->jam_buka }} </p>
                <p align="justify"> <b> Fasilitas : </b> {{ $restoran->fasilitas }}</p> <br>
                <div class="col-lg-10">
                    <a href="/review/{{ $restoran->restaurantID }}" style="margin: auto; display:block;" class="btn btn-warning btn-lg btn-block rounded btn-sm col-3 mb-4">Tambah Review</a>
                    <h3 class="font-weight-light mt-3">Read what others have said about us: </h3> <br>
                    @forelse ($review as $key => $review)
                    <div class="card">
                        <div class="card-header"> {{$review->name}}
                        </div>
                        <div class="card-body">
                            <blockquote class="blockquote mb-0">
                                <p>{{$review->komentar}}</p>
                                <p> <small class="text-muted text-black">{{$review->created_at}}</small> </p>
                            </blockquote>
                        </div>
                    </div>
                    @empty
                        <div class="col-lg-12">
                            <div class="d-block d-md-flex listing">
                                <div class="lh-content">
                                    <div class="alert alert-danger text-center" role="alert">
                                        Review Tidak Ditemukan <br><br>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforelse
                </div>
            </div>
        </div>
    </div>
</div>

@include('templates.footer')
