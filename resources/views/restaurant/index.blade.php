@include('templates.header')
<div class="site-blocks-cover inner-page-cover overlay" style="background-image:url('../../assets/images/register.jpg');" data-aos="fade" data-stellar-background-ratio="0.5">
    <div class="container">
        <div class="row align-items-center justify-content-center text-center">
            <div class="col-md-10" data-aos="fade-up" data-aos-delay="400">
                <div class="row justify-content-center mt-5">
                    <div class="col-md-8 text-center">
                        <h1>Daftar Restaurant</h1>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="site-section bg-light">
    <div class="container">
        <div class="row mb-5">
            <div class="col-lg-12">
                <form method="get" action="{{ route('restaurant.search') }}">
                    <div class="input-group mb-3">
                        <input type="text" class="form-control" name="keyword" placeholder="Cari Restaurant">
                        <div class="input-group-append">
                            <button class="btn btn-primary" type="submit">Search</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        @if(session('success'))
        <div class="col-lg-12">
            <div class="alert alert-success alert-dismissible fade show" role="alert">
                {{session('success')}}
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        </div>
        @endif
        <a href="/restaurant/tambahRestaurant" class="btn btn-warning btn-lg btn-block rounded btn-sm col-3 ml-auto">Tambah Restaurant</a>
        <div class="row mt-5">
            <div class="col-lg-12">
                <div class="d-block listing">
                    @forelse ($restaurant as $key => $restaurant)
                        <div class="d-block d-md-flex listing">
                            <a href="/restaurant/{{ $restaurant->restaurantID }}" class="img d-block" style="background-image: url('../../uploads/restaurant/{{$restaurant->image}}');"></a>
                            <div class="lh-content">
                                <h2><a href="/restaurant/{{ $restaurant->restaurantID }}" class="text-warning">{{$restaurant->nama_restaurant}}</a></h2>
                                <address>Jln. {{$restaurant->alamat}} <br> Kota {{$restaurant->kota}}</address>
                                <a href="/restaurant/{{ $restaurant->restaurantID }}/edit" class="ml-1 btn btn-success btn-sm float-right">Edit</a>
                                <button class="btn btn-danger btn-sm float-right remove-rest" data-id="{{ $restaurant->restaurantID }}" data-action="{{ route('restaurant.destroy', $restaurant->restaurantID) }}"> Delete</button>
                                <a href="/review/{{ $restaurant->restaurantID }}" class="ml-1 mr-1 btn btn-info btn-sm float-right">Review</a>
                            </div>
                        </div>
                        @empty
                        <div class="col-lg-12">
                            <div class="d-block d-md-flex listing">
                                <div class="lh-content">
                                    <div class="alert alert-danger text-center" role="alert">
                                        Restaurant Tidak Ditemukan <br><br>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforelse
                </div>
            </div>
        </div>
    </div>
</div>

@push('scripts')
<script type="text/javascript">
  $("body").on("click",".remove-rest",function(){
    var current_object = $(this);
    swal({
        title: "Are you sure?",
        text: "You will not be able to recover this imaginary file!",
        type: "error",
        showCancelButton: true,
        dangerMode: true,
        cancelButtonClass: '#DD6B55',
        confirmButtonColor: '#dc3545',
        confirmButtonText: 'Delete!',
    },function (result) {
        if (result) {
            var action = current_object.attr('data-action');
            var token = jQuery('meta[name="csrf-token"]').attr('content');
            var id = current_object.attr('data-id');
            $('body').html("<form class='form-inline remove-form' method='post' action='"+action+"'></form>");
            $('body').find('.remove-form').append('<input name="_method" type="hidden" value="delete">');
            $('body').find('.remove-form').append('<input name="_token" type="hidden" value="'+token+'">');
            $('body').find('.remove-form').append('<input name="id" type="hidden" value="'+id+'">');
            $('body').find('.remove-form').submit();
        }
    });
});
</script>
@endpush

@include('templates.footer')
