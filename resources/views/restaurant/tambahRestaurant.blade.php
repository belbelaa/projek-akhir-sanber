@include('templates.header')

<div class="site-blocks-cover inner-page-cover overlay" style="background-image:url('../../assets/images/register.jpg');" data-aos="fade" data-stellar-background-ratio="0.5">
    <div class="container">
        <div class="row align-items-center justify-content-center text-center">
            <div class="col-md-10" data-aos="fade-up" data-aos-delay="400">
                <div class="row justify-content-center mt-5">
                    <div class="col-md-8 text-center">
                        <h1>Tambah Restaurant</h1>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<form role="form" action="/restaurant" method="POST" enctype="multipart/form-data">
    @csrf
    <div class="site-section bg-light">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-md-7 mb-5" data-aos="fade">
                    <div class="row form-group">
                        <div class="col-md-12">
                            <label for="nama" class="text-black">Nama</label>
                            <input type="text" id="nama" name="nama" value="{{ old('nama', '') }}" class="form-control" placeholder="Nama Restoran" required>
                            {{-- @error('nama_restaurant')
                                <div class="alert alert-danger">{{ $message }}</div>
                            @enderror --}}
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col-md-12">
                            <label for="alamat" class="text-black">Alamat</label>
                            <input type="text" id="alamat" name="alamat" value="{{ old('alamat', '') }}" class="form-control" placeholder="Alamat Restaurant" >
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col-md-12">
                            <label for="kota" class="text-black">Kota</label>
                            <input type="text" id="kota" name="kota" value="{{ old('kota', '') }}" class="form-control" placeholder="Kota Restaurant" >
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col-md-12">
                            <label for="jambuka" class="text-black">Jam Buka</label>
                            <input type="text" id="jambuka" name="jambuka" value="{{ old('jambuka', '') }}" class="form-control" placeholder="Jam Buka Restaurant" >
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col-md-12">
                            <label for="fasilitas" class="text-black">Fasilitas</label>
                            <input type="text" id="fasilitas" name="fasilitas" value="{{ old('fasilitas', '') }}" class="form-control" placeholder="Fasilitas Restaurant" >
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col-md-12">
                            <label for="telepon" class="text-black">Nomor Telepon</label>
                            <input type="text" id="telepon" name="telepon" value="{{ old('telepon', '') }}" class="form-control" placeholder="Nomor Telepon Restaurant" >
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col-md-12">
                            <label for="deskripsi" class="text-black">Deskripsi</label>
                            <textarea type="text" id="deskripsi" name="deskripsi" value="{{ old('deskripsi', '') }}" rows="10" cols="50" class="form-control" placeholder="Deskripsi Restaurant" >
                            </textarea>
                        </div>
                    </div>
                    <div class="row input-group">
                        <div class="col-md-12">
                            <div class="custom-file">
                                <input type="file" name="image" id="image" class="custom-file-input">
                                <label for="image" class="custom-file-label">Choose File</label>
                            </div>
                        </div>
                    </div>
                    <div class="row form- mt-3">
                        <div class="col-md-12">
                            <input type="submit" value="Daftar" name="daftar" class="btn btn-warning py-2 px-4 text-white">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</form>

@include('templates.footer')
