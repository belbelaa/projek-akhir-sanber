@include('templates.header')

<div class="site-blocks-cover inner-page-cover overlay" style="background-image:url('../../assets/images/register.jpg');" data-aos="fade" data-stellar-background-ratio="0.5">
    <div class="container">
        <div class="row align-items-center justify-content-center text-center">
            <div class="col-md-10" data-aos="fade-up" data-aos-delay="400">
                <div class="row justify-content-center mt-5">
                    <div class="col-md-8 text-center">
                        <h1>Review Restaurant</h1>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="site-section bg-light">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-9 mb-5" data-aos="fade">
                <form action="#" class="p-5 bg-white" action="#" method="post" enctype="multipart/form-data">
                @csrf
                <h3 style="text-align: center" class="mb-3">Restaurant {{$restoran->nama_restaurant}}</h3>
                <!-- <img src="{{ asset('/img/logo.png') }}" class="rounded mx-auto d-block" alt=""> -->
                    <div class="row form-group">
                        <div class="col-sm-12">
                            <!-- textarea -->
                            <div class="form-group">
                                    <label class="text-black">Komentar Anda</label>
                                    <textarea type="text" class="form-control @error('komentar') is-invalid @enderror" name="komentar" rows="5" value="{{ old('komentar') }}" placeholder="Masukkan komentar. . ."></textarea>
                                    @error('komentar')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                            </div>
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col-md-12">
                            <input type="submit" value="Submit" class="btn btn-warning py-2 px-4 text-white">
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

@include('templates.footer')