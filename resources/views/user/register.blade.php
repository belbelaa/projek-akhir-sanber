@include('templates.header')

<div class="site-blocks-cover inner-page-cover overlay" style="background-image:url('../../assets/images/register.jpg');" data-aos="fade" data-stellar-background-ratio="0.5">
    <div class="container">
        <div class="row align-items-center justify-content-center text-center">
            <div class="col-md-10" data-aos="fade-up" data-aos-delay="400">
                <div class="row justify-content-center mt-5">
                    <div class="col-md-8 text-center">
                        <h1>Register</h1>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="site-section bg-light">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-7 mb-5" data-aos="fade">
                <form action="#" class="p-5 bg-white" action="{{ route('register') }}" method="post" enctype="multipart/form-data">
                @csrf
                    <div class="row form-group">
                        <div class="col-md-12">
                            <label class="text-black">Username</label>
                            <input type="text" name="name" placeholder="Username"  class="form-control @error('name') is-invalid @enderror" value="{{ old('name') }}">
                            @error('name')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col-md-12">
                            <label class="text-black">Email</label>
                            <input type="text" name="email"  class="form-control @error('email') is-invalid @enderror" placeholder="Email" value="{{ old('email') }}">
                            @error('email')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>

                    <div class="row form-group">
                        <div class="col-md-12">
                            <label class="text-black">Password</label>
                            <input type="password" name="password"  class="form-control @error('password') is-invalid @enderror" placeholder="Password">
                            @error('password')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>

                    <div class="row form-group">
                        <div class="col-md-12">
                            <label class="text-black">Repeat Password</label>
                            <input type="password" name="password_confirmation"  id="password-confirm" class="form-control @error('password') is-invalid @enderror" placeholder="Repeat Password">
                            @error('password')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>

                    <div class="row form-group">
                        <div class="col-12">
                            <p>Punya akun? <a href="/login" class="text-warning">Log In</a>
                            </p>
                        </div>
                    </div>
                    <div class="row form-group">
                        <div class="col-md-12">
                            <input type="submit" value="Sign Up" class="btn btn-warning py-2 px-4 text-white">
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

@include('templates.footer')
